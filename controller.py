############## Audience Insights ###############

from Connectors.ElasticConnector import Get as ESget
from config import _AUDIENCE_SOURCE, KNOWLEDGE_GRAPH_KEY
from Engine.TextGrip import tg
from Engine.WatsonAPI import WAPI
from Engine.KnowledgeGraph import KnowledgeGraph

# Get Audience Texts
get = ESget(_AUDIENCE_SOURCE)
messages_raw = get._get_stream(_AUDIENCE_SOURCE, _source = ["m"])
messages_raw = [_["m"] for _ in messages_raw if "m" in _]
corpus_raw = ". ".join(messages_raw)

messages_cleaned = get._get_stream(_AUDIENCE_SOURCE, _source = "cleaned_post")
messages_cleaned = [_["cleaned_post"] for _ in messages_cleaned if "cleaned_post" in _]
corpus_cleaned = ". ".join(messages_cleaned)

# Vocabulary Analysis 
# vocab = tg.vocabulary_analysis(corpus_cleaned)

# Top Mentioned
# mentioned = []
# mentioned = tg.mentioned_distribution(corpus_cleaned, mentioned)

# Noun Phrases
# noun_phrases = tg.phrases(corpus_raw)

# Top Questions Asked 
# questions = tg.top_questions_asked(messages_raw)[:20]:

# Comphrensive Comments 
# commp = tg.comphrensive_comment(messages_raw)

# Emojis Used
# emojis = tg.emoticon_distribution(corpus_raw)

# Audience Tone # $$ TODO - Aggreagate Over Corpuses $$ #
# corpus_list = WAPI.chunks(messages_raw, 999)
# for corpus in corpus_list:
# 	corpus_text = ". ".join(corpus)
# 	print WAPI.tone_analyzer(corpus_text)

# Top Ngrams 
# ngram_n = 2
# ngram_limit = 100
# tng = tg.top_ngrams(corpus_cleaned, ngram_n)[:ngram_limit]

# Knowledge Graph Information Extraction
# kg = KnowledgeGraph(KNOWLEDGE_GRAPH_KEY)
# types = []
# names = []
# for each in tng:
# 	res = kg.knowledge_graph(each[0])
# 	if not res:
# 		continue
	
# 	entity = res 
# 	if entity['score'] >= 100:
# 		_type = entity['type']
# 		_name = entity['name']
# 		types.extend(_type)
# 		names.extend(_name)
# types = Counter(types)
# types = sorted(types.items(), key = operator.itemgetter(1), reverse = True)

# names = Counter(names)
# names = sorted(names.items(), key = operator.itemgetter(1), reverse = True)


### $$ TODO -  Sarcastic Comments 
### $$ TODO - SENTIMENT AND DRIVERS 
### $$ POS TAG ANALYSIS 
### $$ Content Analyzer Stuff - Coccurance + Suggestions etc 

### AGGREGATIONS - DB Level