print "Connecting"
from Engine.TextAnnotator import AnnotateDocument as AD
from Connectors.ElasticConnector import Get as ESget, Add as ESadd, Aggregator as ESagg

from config import _SOURCE, QUERY_MUST
from config import *


''' Annotation and Bulk Update'''
fetch_config = {
    'must_query' : QUERY_MUST,
    'must_not' : QUERY_MUST_NOT,
    'size' : ES_SCROLL_LIMIT,
    'scroll' : ES_SCROLL_VALUE,
}

bulk_body = ""

add = ESadd(_SOURCE)
get = ESget(_SOURCE)

print "Fetching Documents from ES using input query"
documents = get._get_stream(fetch_config)

for document in documents:
	print 'Annotating Documents'
	pos = AD.annotate_document(document)

	if "sentiment" in pos:
		del pos['sentiment']
	pos['_index'] = "shivam_test"

	try:
		print "Adding Document"
		add.add_document(pos)
	except Exception as E:
		print E 
		continue 


	''' BULK UPDATE '''
	# bulk_body += '{ "update" : {"_id" : "'+pos['id']+'", "_index" : "' + pos['_index'] + '", "_type" : "propheseePost"} }\n'
	# bulk_body += '{ "script": "ctx._source.dimension = ' + str(dimension) +'"}\n'

	#if len(bulk_body.split("\n")) % ELASTIC_UPDATE_SIZE == 1:
		# update._bulk_operation(bulk_body)
		# bulk_body = ""



''' Aggregation - Univariate + Multivariate + Bucketing '''
# agg = ESagg(_SOURCE)
# print agg.univariate_analysis('hT', QUERY_MUST)
# print agg.univariate_analysis('sent', QUERY_MUST, 'CONTINUOUS', aggregation_function = 'percentiles', agg_func_param = [98])

# a = [{ "from" : -1, "to" : 1 }]
# biResults = agg.bivariate_analysis('mUsr.username', 'hT', QUERY_MUST, agg_key_func = 'terms', aggkey_func_param = None)
# print biResults

# multiResults = agg.multivariate_analysis(['mUsr.username', 'hT',age], 'sent', QUERY_MUST, aggkey_func = {'func':'max'})
# print multiResults

# key2 = "snSt.likes"
# key1 = "hT"
# bucketing_config = {
# 	key1 : None,
# 	key2 : {
# 			'window' : 2,
# 			'scale' : 1
# 	}
# }
# biResults = agg.bivariate_analysis(key1, key2, QUERY_MUST, getAverage = False)
# results = agg.parse_bivariate(biResults, bucketing_config)