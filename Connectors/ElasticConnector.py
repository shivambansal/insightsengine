import elasticsearch

class Get:
	def __init__(self, _SOURCE):
		self._SOURCE = _SOURCE
		self.source_es = elasticsearch.Elasticsearch([{'host':self._SOURCE['ELASTIC_HOST'],'port':self._SOURCE['ELASTIC_PORT']}])

	def _get_document_count(self, _config):
		_input_query = { "query": { "bool": { "must": _config['MUST_QUERY'] }}}

		res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = _input_query, scroll = _config['SCROLL_VALUE'], size = _config['SCROLL_SIZE'])
		return res['hits']['total']

	def _get_stream(self, _config, _source = None):
		if "MUST_NOT_QUERY" in _config:
			_input_query = { "query": { "bool": { "must_not" : _config['MUST_NOT_QUERY'], "must": _config['MUST_QUERY'] }}}
		else:
			_input_query = { "query": { "bool": { "must": _config['MUST_QUERY'] }}}
		
		if _source:
			res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = _input_query, _source = _source, scroll = _config['SCROLL_VALUE'], size = _config['SCROLL_SIZE'])
		else:
			res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = _input_query, scroll = _config['SCROLL_VALUE'], size = _config['SCROLL_SIZE'])


		scroll_size = len(res['hits']['hits'])
		while (scroll_size > 0):
			for post in res['hits']['hits']:
				
				pos = post['_source']	
				pos['_index'] = post['_index']		
				yield pos

			scroll_id = res['_scroll_id']
			res = self.source_es.scroll(scroll_id = scroll_id, scroll = _config['SCROLL_VALUE'])
			scroll_size = len(res['hits']['hits'])

class Add:
	def __init__(self, _SOURCE):
		self._SOURCE = _SOURCE
		self.source_es = elasticsearch.Elasticsearch([{'host':self._SOURCE['ELASTIC_HOST'],'port':self._SOURCE['ELASTIC_PORT']}])

	def add_document(self, document):
		idd  = document['id']
		print idd 
		del document['id']
		self.source_es.index(index = document['_index'], doc_type = 'propheseePost', id = idd, body = document)

class Update:
	def _update_document(self, dimension, _id, _index):
		try:
			self.source_es.update(index = _index, id = _id, doc_type = "propheseePost", body = {"doc": {"dimension": dimension}})
		except Exception as E:
			print "Update Failed due to - ", E 
			pass 

	def _bulk_operation(self, bulk_data):
		try:
			self.source_es.bulk(body = bulk_data)
		except Exception as E:
			print "Bulk Update Failed due to - ", E
			pass

class Aggregator:
	def __init__(self, _SOURCE):
		self._SOURCE = _SOURCE
		self.source_es = elasticsearch.Elasticsearch([{'host':self._SOURCE['ELASTIC_HOST'],'port':self._SOURCE['ELASTIC_PORT']}])



	def univariate_analysis(self, key, must_query, type_var = 'CATEGORICAL', aggregation_function = 'terms', agg_func_param = None, limit = 0):
		'''
		aggregation_function - terms (default), other options - min, max, avg, range, percentiles
		agg_func_param =  [{ "to" : 50 }, { "from" : 50, "to" : 100 }, { "from" : 100 }], percentiles - [95, 99, 99.9] 

		TODOS 

		'''

		filter_query = { "query": { "bool": { "should": { "bool": { "must": must_query }}, "minimum_should_match": "1" }}}
		
		if type_var == 'CONTINUOUS':
			aggregation_query = { "aggs": {"_id": { aggregation_function: self._gen_params(aggregation_function, key, agg_func_param, limit)}}}
		else:
			aggregation_query = { "aggs": {"_id": { aggregation_function: { "field": key, "size":limit }}}}

		input_query = {}
		input_query.update(filter_query)
		input_query.update(aggregation_query)

		res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = input_query)
		if res['hits']['total'] > 0:
			result =  res['aggregations']['_id']
		else:
			result = []	
		return result


	def bivariate_analysis(self, key, agg_key, must_query, key_func = 'terms', agg_key_func = 'terms', key_func_param = None, aggkey_func_param = None, limit = 0):
		filter_query = { "query": { "bool": { "should": { "bool": { "must": must_query }}, "minimum_should_match": "1" }}}

		aggregation_query = { 
							  "aggs" : { "_id1" : { key_func : self._gen_params(key_func, key, key_func_param, limit),
							  "aggs" : { "_id2" : { agg_key_func : self._gen_params(agg_key_func, agg_key, aggkey_func_param)}}}}
                      		}

		input_query = {}
		input_query.update(filter_query)
		input_query.update(aggregation_query)

		res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = input_query)
		result =  res['aggregations']['_id1']['buckets']
		return result


	def _gen_params(self, func, key, params, limit = None):
		agg_obj = { 'field' : key }
		
		if limit:
			agg_obj['size'] = limit
		
		if func == 'range':
			agg_obj['ranges'] = params
		
		elif func == 'percentiles' and params != None:
			agg_obj['percents'] = params
		
		return agg_obj

	def _finditem(self,obj, key):
		if key in obj: return obj[key]
		items_list = obj.items()
		items_list.reverse()
    		for k, v in items_list:
        		if isinstance(v,dict):
        			return self._finditem(v, key) 
	
	def _retfunc(self,key,keys_func):
	    	func = 'terms'
	    	params = None    
	    	if keys_func is not None:
	        	if key in keys_func.keys():
	            		func = keys_func[key]['func']
	            	if 'params' in keys_func[key]:
	                	params = keys_func[key]['params']
	    	return func,params
	
	def _retaggfunc(self,agg_func):
	    	func = 'terms'
	    	params = None    
	    	if agg_func is not None:
			func = agg_func['func']
		       	if 'params' in agg_func:
		            	params = agg_func['params']
	    	return func,params     



	# '''
    
	#     keys = [key1,key2,key3...]
	#     keys_func = {'key1':{'func':'terms','params':None},'key2':{'func':'percentiles','params':[50,75]}}
	    
	#     ===> by default func will be terms for each key
	    
	#     aggkey = 'key4'  ==> key to perform aggregation on
	#     aggkey_func = {'func':'avg','params':[]}
	        

	# '''

	def multivariate_analysis(self,keys, aggkey,must_query, keys_func=None,aggkey_func=None,limit = 0):
		filter_query = { "query": { "bool": { "should": { "bool": { "must": must_query }}, "minimum_should_match": "1" }}}

		len_keys = len(keys)
		aggregation_query = {}
		func1,params1 = self._retfunc(keys[0],keys_func)
		aggregation_query['aggs'] = {'_id0':{func1: self._gen_params(func1,keys[0],params1)}}       
		for i in range(1,len_keys):
			ref_key = self._finditem(aggregation_query,'_id'+str(i-1))
			func,params = self._retfunc(keys[i],keys_func)

			ref_key['aggs'] = {"_id"+str(i): {func: self._gen_params(func,keys[i],params)}}

		aggfunc,aggparams = self._retaggfunc(aggkey_func)
		ref_key = self._finditem(aggregation_query,'_id'+str(len_keys-1))
		ref_key['aggs'] = {"_id"+str(len_keys+1): {aggfunc: self._gen_params(aggfunc,aggkey,aggparams)}}    
		input_query = {}
		input_query.update(filter_query)
		input_query.update(aggregation_query)
		print input_query
		res = self.source_es.search(index = self._SOURCE['ELASTIC_INDEX'], body = input_query)
		result =  res['aggregations']['_id0']
		return result
    
         
	# def parse_bivariate(self, results):
	# 	for each in results:
	# 		for bucket in each['_id2']['buckets']:
	# 			print each['key'], bucket['key'], bucket['doc_count']

	def parse_bivariate(self, results, bucketing_config):
		for each in results:
			# print each['key']
			# raw_key = each['_id2']['buckets']['key']
			# raw_count = each['_id2']['buckets'][doc_count]	


			for bucket in each['_id2']['buckets']:
				doc = []
				doc.append(each['key'])
				doc.append(bucket['key'])
				doc.append(bucket['doc_count'])
				print doc 

		# 		results.append(doc)
		# return results

def bucket_range(adict, bucket_size):
	minV = int(min(adict.keys())) 
	maxV = int(max(adict.keys())) 
	lower = [x for x in range(minV, maxV, bucket_size)]
	upper = [x+bucket_size for x in lower]

	bucketed = {}
	for key, value in adict.iteritems():
		for i, lw in enumerate(lower):
			if int(key) >= lw and upper[i]:
				bucket = str(lw) + "-" + str(upper[i])
				if bucket not in bucketed:
					bucketed[bucket] = 0
				bucketed[bucket] += int(value)
	return bucketed
