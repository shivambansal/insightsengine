from pymongo import MongoClient 

class Add:
	def __init__(self, config):
		HOST = config['MONGO_HOST']
		PORT = config['MONGO_PORT']
		self.db = MongoClient(HOST, PORT)[config['MONGO_DATABASE_NAME']]


	def add_document(self, doc, collname):
		try:
			self.db[collname].insert(doc)
		except Exception as E:
			print "Addition Failed:", E 
			 

class Get:
	def __init__(self, config):
		HOST = config['MONGO_HOST']
		PORT = config['MONGO_PORT']
		self.db = MongoClient(HOST, PORT)[config['MONGO_DATABASE_NAME']]

	def get_documents(self, collname):
		return self.db[collname].find()

	def get_all_keys(self, collname):
		return self.db[collname].find_one().keys()

	def get_values(self, collname, key):
		curs = self.db[collname].find({}, {key:1, '_id':0})
		curs = [x[key] if key in x else "" for x in curs]
		return curs

	def cursor_to_list(self, cursor):
		return 	[_ for _ in cursor]


	def get_top_values(self, collname, key, top_value, order, limit):
		res = self.db[collname].find({}, {'_id' : 0, key : 1, top_value : 1}).sort(key, order).limit(limit)
		res = self.cursor_to_list(res)
		return res


	def get_values_wind(self, key, collname):
		pipe = [{'$unwind' : '$'+ key}, 
				{'$group' : {'_id' : '$'+key, 'freq' : {'$sum':1}}},
				{'$sort':{'freq':-1}}]
		res = self.db[collname].aggregate(pipe)
		res = self.cursor_to_list(res)
		return res 


	def univariate_analysis(self, key, group_key, collname, limit = False, sorting_order = "DESC", central_tendencies = False):
		''' 
		Variable Analysis - Univariate 
		Function to perform univariate analysis on a variable. Works directly for Categorical Variables. For Continuous
		Variables, one can use binning function first before univariate analysis. 
		
		params:
		key: Name of the key (variable) 
		collname: Name of the collection which contains the documents
		limit: Number of documents / rows to be analysed, Default is False (all documents)
		sorting_order: Arranging the results in asending or descending order (ASEC or DESC)
		central_tendencies: Boolean, True if you want to include mean, median and mode in the results
		'''

		sorter = -1
		if sorting_order != "DESC":
			sorter = 1			

		if central_tendencies:
			pipe = [{'$group' : {'_id' : '$'+key, 'freq' : {'$sum':'$'+group_key}, 'mean':{'$avg':'$'+group_key}, 'min':{'$min':'$'+group_key}, 'max':{'$max':'$'+group_key}}}, 
					{'$sort':{'freq':sorter}}]
		else:
			pipe = [{'$group' : {'_id' : '$'+key, 'freq' : {'$sum':1}}},
					{'$sort':{'freq':sorter}}]

		if limit:
			pipe.append({'$limit':limit})
		res = self.db[collname].aggregate(pipe)
		res = self.cursor_to_list(res)
		return res 

	def bivariate_analysis(self, key1, key2, group_key, collname, limit = False, sorting_order = "DESC"):
		''' 
		Variable Analysis - BiVariate 
		Function to perform bivariate analysis on a variable. 
		
		params:
		key1: Name of the key1 (variable)
		key2: Name of the key2 (variable) 
		collname: Name of the collection which contains the documents
		limit: Number of documents / rows to be analysed, Default is False (all documents)
		sorting_order: Arranging the results in asending or descending order (ASEC or DESC)
		'''

		sorter = -1
		if sorting_order != "DESC":
			sorter = 1

		pipe = [{'$group' : {'_id' : {'key1':'$'+key1,'key2':'$'+key2}, 'freq' : {'$sum':1}, 'mean':{'$avg':1}, 'min':{'$min':1}, 'max':{'$max':1} }}, 
				{'$sort':{'freq':sorter}}]
		if limit:
			pipe.append({'$limit':limit})
	
		res = self.db[collname].aggregate(pipe)
		res = self.cursor_to_list(res)
		return res 

	def multivariate_analysis(self, combination, group_key, collname, limit = False, sorting_order = "DESC"):
		group_id = {}
		for i, comb in enumerate(combination):
			group_id[str(comb)] = "$"+comb 

		sorter = -1
		if sorting_order != "DESC":
			sorter = 1

		pipe = [{'$group' : {'_id' : group_id, 'freq' : {'$sum':'$'+group_key}, 'mean':{'$avg':'$'+group_key}, 'min':{'$min':'$'+group_key}, 'max':{'$max':'$'+group_key} }}, 
				{'$sort':{'mean':sorter}}]
		
		if limit:
			pipe.append({'$limit':limit})
	
		res = self.db[collname].aggregate(pipe)
		res = self.cursor_to_list(res)
		return res 		

add = Add()
get = Get()