# -*- coding: utf-8 -*-
"""
Created on Fri Oct  7 15:45:22 2016

@author: aman
"""

import pandas as pd
import numpy as np
import collections
import random



class GenerateInsights:
    
    def __init__(self):
        self.df = pd.read_csv('InsightsEngine/dictionaries/insights/insights_v1.csv')
        # self.df = pd.read_csv('dictionaries/insights/insights_v1.csv')
    
    def get_message(self,key,value):
        df = self.df.copy()
        df = df[df['feature']==key]
        df = df[(df['lower']<=value) & (df['upper']>=value)]
        if len(df)>0:
            df = df.reset_index()

            results = []
            for x in df.values:
                results.append(x)
            return results

            # insight = df.ix[0,'insight']
            # suggestion = df.ix[0,'suggestion']
            # section = df.ix[0,'section']
            # return insight,suggestion,section
        else:
            return []

    def flatten(self,d, parent_key='', sep='_'):
        items = []
        for k, v in d.items():
            new_key = parent_key + sep + k if parent_key else k
            if isinstance(v, collections.MutableMapping):
                items.extend(self.flatten(v, new_key, sep=sep).items())
            else:
                items.append((new_key, v))
        return dict(items)

    def parse_insights(self,data):
        data['insights'] = {}
        results = self.flatten(data, parent_key='', sep='.')
        for key,value in results.items():
            if type(value) == float or type(value) == int:
                key = key.split('.')[-1]
                if key in list(self.df['feature']):
                    
                    results = self.get_message(key,value)
                    if results:

                        sec = results[0][3]
                        insights = [_[7] for _ in results]
                        suggestions = [_[8] for _ in results]
                        
                        random.shuffle(insights)
                        random.shuffle(suggestions)

                        ins = insights[0]
                        sug = suggestions[0]

                        if sec not in data['insights']:
                            data['insights'][sec] = []
                            if sec:
                                r = {key:{}}
                                if ins!=None and str(ins)!='None':
                                    r[key]['insights'] = ins
                                if sug!= np.nan and sug!=None and str(sug)!='None':
                                    r[key]['suggestion'] = sug
                                data['insights'][sec].append(r)
        return data