import requests 


start_date = "2016-01-01"

url = "https://api.prophesee.in/graph/dashboard/MissMalini?fromDate=2016-01-01&toDate=2016-09-26&"\
										"facebook=101827586822&"\
										"twitter=16983244&"\
										"instagram=269548788"
										
headers = {
	'email' : 'missmalini@prophesee.in',
	'Authorization' : 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbWFpbEFkZHJlc3MiOiJtaXNzbWFsaW5pQHByb3BoZXNlZS5pbiIsIm5hbWUiOiJNaXNzIE1hbGluaSIsImlhdCI6MTQ3NDc1NjU0OH0.EUT-1wERIof9NnU1JtCfSdtabz8NUHYZt67HZXc1mI4',
	'Content-Type' : "application/json"
	}
resp = requests.get(url, headers = headers)

def chunks(l, n):
	for i in range(0, len(l), n):
		yield l[i:i + n]

i = 0
dat = []
weeks = {}
days = resp.json()['facebook'][1:-93]


sections = []
for each in chunks(days, 7):
	sections.append(sum(each))



def linreg(X, Y):
    N = len(X)
    Sx = Sy = Sxx = Syy = Sxy = 0.0
    for x, y in zip(X, Y):
        Sx = Sx + x
        Sy = Sy + y
        Sxx = Sxx + x*x
        Syy = Syy + y*y
        Sxy = Sxy + x*y
    det = Sxx * N - Sx * Sx
    return (Sxy * N - Sy * Sx)/det, (Sxx * Sy - Sx * Sxy)/det


for x in chunks(sections,12):
	print x 
	
	x = [int(z) for z in x]
	a,b = linreg(range(len(x)),x)  

	extrapolatedtrendline = [a*index + b for index in range(len(x) + 3)][-3] 
	print extrapolatedtrendline
	print


# Add Expnonetial Weighted Moving Average 