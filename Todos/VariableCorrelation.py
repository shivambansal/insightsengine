import numpy as np


class Correlation:
	def get_correlation(self, document):
		values = document.values()

		# Add uniformity in values 
		a = np.array([ [0.1, .32, .2,  0.4, 0.8], [.23, .18, .56, .61, .12], [.9,   .3,  .6,  .5,  .3],  [.34, .75, .91, .19, .21]]) 
		b = np.corrcoef(a)

		print b 