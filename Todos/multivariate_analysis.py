from dbHandler import add, get 
import math
from collections import Counter
import operator
import itertools

def get_benchmarking(values):
	values = sorted(values)
	avg1 = float(sum(values)) / len(values)

	# Do this if top 10% values have high variation
	ran = int(math.ceil(float(len(values)) / 10))
	values = values[:-ran]
	
	avg2 = float(sum(values)) / len(values)
	seventy_five_percent = float(avg2 + values[-1]) / 2
	return avg2, seventy_five_percent

def filter_buckets(values):
	filtered = []
	for each in values:
		valid = True
		for k,v in each['_id'].iteritems():
			if k in keys and v < keys[k]: 	
				valid = False
		if valid:
			filtered.append(each) 
	return filtered

def insight_buckets(values):
	# Univariate Insights after intersection
	# Multivariate Insights after intersection
	for each in values:
		print each['_id'], each['freq']


keys = {
	# 'post_mentions_len' : 1,
	# 'post_hashtags_len' : 1,
	

	# 'hour' : 1,
	# 'date' : 1,
	# 'day' : 1,
	

	# 'sent' : 1,
	# 'language' : 1,


	# 'post_power_len' : 1,
	# 'post_positives_len' : 1,
	# 'post_emotional_len' : 1,
	# 'post_negatives_len' : 1,
	

	# 'post_words_len' : 1,
	# 'post_sentence_length' : 1,
	# 'avg_letter_per_word' : 1,
	# 'flesch_kincaid_grade' : 1,
	# 'flesch_reading_ease' : 1,
	# 'avg_syllables_per_word' : 1,
	# 'syllable_count' : 1,
	# 'text_standard' : 1,
	
	
	# 'last_trigram' : 1,
	# 'last_bigram' : 1,
	# 'first_bigram' : 1,
	# 'last_unigram' : 1,
	'first_unigram' : 1,
	# 'first_trigram' : 1


	# 'Adverb' : 1,
	# 'Verb' : 1,
	# 'Noun' : 1,
	# 'CommonTag' : 1,
	# 'Adjective' : 1,
	# 'Special' : 1,
}

t1 = "engagements"
collname = "RioCampaign"

values = get.get_values(collname, t1)
avg, seventy_five_percent = get_benchmarking(values)

print "Generating Combinations"
all_insights = []
for combo_length in range(1, len(keys)+1):
	# Generate All combos of combo_length

	for combination in itertools.combinations(keys, combo_length):
		print "Testing Combination - ", combination
		# For a combination, aggregate the data 
		combination = list(combination)
		response = get.multivariate_analysis(combination, t1, collname)	

		# above_average = [x for x in response if x['freq'] >= avg]

		good_performance = [x for x in response if x['mean'] >= seventy_five_percent]

		good_performance = filter_buckets(good_performance)
		all_insights.extend(good_performance)

print "Generating Unigrams and Multigram Combinations"
# Try to make sense 
uni_insights = {}
alls = []
for each in all_insights:

	id_keys = each['_id'].keys()
	id_vals = each['_id'].values()
	k1 = " - ".join([str(d)+" "+str(b) for d,b in zip(id_keys, id_vals)])
	alls.append(k1) 


	for key, value in each['_id'].iteritems():
		# Univariate Insights 		
		if key not in uni_insights:
			uni_insights[key] = []
		uni_insights[key].append(value)

for x,y in uni_insights.iteritems():
	vals = Counter(y) 
	vals = sorted(vals.items(), key = operator.itemgetter(1), reverse = True)
	vals = [list(A) for A in vals]

	# Ideal Values
	print x, vals[0]


visited = {}
for i, each in enumerate(alls):
	temp = alls[:i] + alls[i+1:]
	for substr in temp:
		if each in substr:
			if each not in visited:
				visited[each] = 1
			else:
				visited[each] += 1 
vals = sorted(visited.items(), key = operator.itemgetter(1), reverse = True)
for each in vals:
	print each

# Insights Wrapper
			
# Posts with words 23 - 24 lead to maximum engagements 
# ideal hashtag length = 1 
# ideal positive words = 1 
# ideal negative_words in the post = 1 



# TODO - Bucketing of variables to get insights 
# 