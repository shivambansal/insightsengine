from dbHandler import get, add, db

from Engine.TextGrip import tg, tgu, ge
from Engine.SentimentDrivers import SD

# CollName1 = "TrumpTweetsNew"
# List of Raw Comments
# posts_raw1 = get.get_values(CollName1, "message")
# corpus_raw1 = ". ".join(posts_raw1)

# List of Proper Comments - PROPER SENTENCES
# posts_cleaned1 = get.get_values(CollName1, "cleaned_post")
# corpus_cleaned1 = ". ".join(posts_cleaned1)

CollName2 = "HillaryTrumpDebates"
# List of Raw Tweets
# posts_raw2 = get.get_values(CollName2, "m")
# corpus_raw2 = ". ".join(posts_raw2)


# List of cleaned tweets
posts_cleaned2 = get.get_values(CollName2, "cleaned_post")
corpus_cleaned2 = ". ".join(posts_cleaned2)
# print corpus_cleaned2
# SD.getDrivers(corpus_cleaned2 )

# corpus_raw = corpus_raw1 +". "+ corpus_raw2
# corpus_cleaned = corpus_cleaned1 + corpus_cleaned2

# print corpus_raw

# listofsentences = posts_raw2 + posts_raw1
# complete_postags = tg.get_complete_postags(listofsentences)
# for each in complete_postags:
# 	print each, complete_postags[each]

# print "__"

# for word in open("temp.txt").read().strip().split("\n"):
# 	print word, complete_postags[word]

# print corpus_cleaned
# Vocabulary Analysis 
# Power | Emotional | Positive | Negative | Common | Uncommon

# va = tg.vocabulary_analysis(corpus_cleaned)
# for key,y in va.iteritems():
# 	if key == "positives": 
# 		for x in y:
# 			print x[0] + "\t" + str(x[1])

# Top N - grams Used 
# print tg.top_ngrams(corpus_cleaned2, 1)[:50]
# tng = tg.top_ngrams(corpus_cleaned2, 2)[:100]
# for x in tng:
	# print x[0] + "\t" + str(x[1]) 

# Mentioned 
# issues = """Immigration
# Abortion
# Gun
# Foreign policy
# Tax
# Gay marriage
# Health care
# Economy and jobs
# Civil Liberties
# Crime and safety
# Environment
# Education
# Budget and spending
# National security
# Medicare and Social Security
# Veterans
# Energy"""
# mentioned = [x.lower() for x in issues.split("\n")]
# # mentioned = ["president obama"]
# print corpus_cleaned2
# print tg.mentioned_distribution(corpus_cleaned2, mentioned)

# Noun Phrases
# for each in tg.phrases(corpus_raw2):
	# print each[0] +"\t"+ str(each[1])

# Top Questions Asked 
# for x in tg.top_questions_asked(comments_proper)[:20]:
# 	print x[0] 
# 	# print

# Comphrensive Comments 
# a = tg.comphrensive_comment(comments_proper)
# for x in a:
	# print x 



# Mongo -----------------

# gen = get.univariate_analysis("cTs", "cTs", CollName2)
# for x in gen:
# 	print x["_id"], x["freq"] 

# Top Positive # Top Scarcastic 
# top = get.get_top_values(CollName, "sentiment", "comment_text", -1, 3)
# for x in top:
# 	if x["scarsam"] > 80 or x["scarsam"] < -80:
	# print x["comment_text"] 

# top = get.get_top_values(CollName, "engagements", "comment_text", -1, 3)
# for x in top:
# 	print x["comment_text"] + "\t" +  str(x["engagements"])


# Top Influencers Score 
# k1 = "date"
# gen = get.univariate_analysis(k1, k1, CollName)
# for x in gen:
	# print str(x["_id"]) + "\t" + str(x["freq"]) 

# type
# Post Words Len 

# Sentiment Distribution 
# sent = get.get_values(CollName, "sentiment")
# neg, pos, neu = 0,0,0
# for x in sent:
# 	if x == "N":
# 		x = 0.0
# 	if x < -0.2:
# 		neg += 1
# 	elif x > 0.2:
# 		pos += 1
# 	else:
# 		neu += 1
# print neg, neu, pos

# Optimism = float(pos) / len(sent)
# Pessimism = float(neg) / len(sent)
# print Optimism
# print Pessimism

# Day Wise Hour Wise CommentCount 
# k1 = "date"
# k2 = "favourite_count"
# bi = get.univariate_analysis(k1, k2, CollName2, central_tendencies = True)
# for x in bi:
	# print x['_id'], x['freq'] 
# dc = {}
# for x in bi:

# 	day = x["_id"]["key1"]
# 	hr = x["_id"]["key2"]
# 	val = x["freq"]

# 	if day not in dc:
# 		dc[day] = {}

# 	if hr not in dc[day]:
# 		dc[day][str(hr)] = val

# days = ["Monday", "Tuesday", "Wednesday","Thursday", "Friday" , "Saturday" , "Sunday"]
# hour = [x for x in range(24)]
# hour = ["0"+str(x) if x < 10 else str(x) for x in hour]
# print hour
# for day in days:
# 	print day + "\t",
# 	for hr in hour:
# 		if hr in dc[day]:
# 			print str(dc[day][hr]) + "\t", 
# 		else:
# 			print "0\t",
# 	print "\n"
	

# Hashtags used by commenters 
# for x in get.get_values_wind("hashtag_entries", CollName2):
	# print "#"+(x['_id']+"\t") + str(x['freq'])

# bi = get.bivariate_analysis()


# Top Influencers Common Interests 
# TOP POSITIVE AND NEGATIVE COMMENTS 

# Overall Grade 
# Tone Analyzer
# Top Concepts | Bot Comments
# Post Comment Velocity
