from TextGrip import tg, tgu, ge

class AnnotateDocument:
	def annotate_document(self, post):
			time_ms = tgu.parse_cTs(post['cTs'])
			post.update(time_ms)

			post['cleaned_post'] = tgu.clean_comment(post["m"])

			post['post_hashtags'] = tgu.get_entitiy_used(post["m"], "#")
			post['post_hashtags_len'] = len(post["post_hashtags"])

			post['post_mentions'] = tgu.get_entitiy_used(post["m"], "@")
			post['post_mentions_len'] = len(post["post_mentions"])

			post['post_words'] = post["m"].split()
			post['post_words_len'] = len(post['post_words'])

			post['post_sentence_length'] = tgu.get_post_sentence_length(post["m"])
			post['post_cleaned_words'] = post['cleaned_post'].split()
			
			post['post_power'] = tg.get_post_words(post['post_cleaned_words'], "power")
			post['post_emotional'] = tg.get_post_words(post['post_cleaned_words'], "emotional")
			post['post_positives'] = tg.get_post_words(post['post_cleaned_words'], "positives")
			post['post_negatives'] = tg.get_post_words(post['post_cleaned_words'], "negatives")

			post['post_power_len'] = len(post["post_power"])
			post['post_emotional_len'] = len(post["post_emotional"])
			post['post_positives_len'] = len(post["post_positives"])
			post['post_negatives_len'] = len(post["post_negatives"])

			post['sarcasm'] = tgu.text_scarsam(post["m"])
			post['sentiment'] = tgu.annotate_sentiment(post["m"])

			readability = tg.readability_analysis(post["m"])
			post.update(readability)

			pos_tags = tg.pos_tags(post["m"])
			post.update(pos_tags)

			post['language'] = tgu.annotate_language(post["m"])

			unigrams = tg.top_ngrams(post["cleaned_post"],1)
			bigrams = tg.top_ngrams(post["cleaned_post"],2)
			trigrams = tg.top_ngrams(post["cleaned_post"],3)

			post['emoticons'] = tg.emoticon_distribution(post['m'])
			post['engagements'] = tgu.get_total_engagements(post['snSt'])

			return post

AnnotateDocument = AnnotateDocument()


	# if analyzeComments and "commentsArray" in pos:
		# 	pass
		# 	''' 	
		# 	# Add Comments Level Data - Need to fix the _id for mongo # Or you can create two versions of modular Code -- Think of wrapping post analyzer in functions
		# 	for comment in pos["commentsArray"]:
		# 		comment_doc = {}	
		# 		comment_doc["commenterId"] = comment['from']['id']
		# 		comment_doc["commenterName"] = comment['from']['name']
		# 		comment_doc["comment_count"] = comment['comment_count']
		# 		comment_doc["like_count"] = comment['like_count']
		# 		comment_doc["comment_id"] = comment["id"]

		# 		if "message" in comment:
		# 			comment_doc["comment_text"] = " ".join(comment['message'].split())
		# 		else:
		# 			comment_doc["comment_text"] = ""

		# 		comment_doc['comment_hashtags'] = tgu.get_hashtags_used(comment_doc["comment_text"])

		# 		language = tgu.annotate_language(comment_doc["comment_text"])
		# 		if language != "Misc":
		# 			sentiment = tgu.annotate_sentiment(comment_doc["comment_text"])
		# 		else:
		# 			sentiment = 0
		# 		comment_doc["comment_language"] = language
		# 		comment_doc["comment_sentiment"] = sentiment[0] 

		# 		comment_doc["cleaned_comment"] = tgu.clean_comment(comment_doc["comment_text"])
		# 		comment_doc['comment_scarsam'] = tgu.text_scarsam(comment_doc['comment_text'])

		# 		comment_doc['comment_engagements'] = comment_doc['like_count'] + comment_doc['comment_count']	
		# 		comment_doc['commenter_gender'] = ge._extract_gender(comment_doc["userName"])
		# 	'''

		# else: