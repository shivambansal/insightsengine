'''
Python Class to perform operations on a bulkier text and extract a number of insights 
corpus - list of sentences 

'''

from SentimentAnalyzer import SentimentAnalyzer
from cleaner import Cleaner 
from textblob import TextBlob
from collections import Counter
from langdetect import detect
import operator 
import requests
import datetime
import calendar
import time
from textstat.textstat import textstat 

class GenderExtractor:
	def _extract_gender(self, name):
		try:
			if " " in name:
				nameis = str(name).strip().replace(" ","/")
			else:
				nameis = str(name) + "/a"
			url = "http://api.namsor.com/onomastics/api/json/gendre/" + nameis
			response = requests.post(url)
			gender = response.json()["gender"]
		except Exception as e:
			gender = "Unknown"
		return gender

class TextGripUtility:
	def __init__(self):
		self.SA = SentimentAnalyzer()

	def annotate_sentiment(self, comment):
		blob = TextBlob(comment)
		sentiment = self.SA.analyzer(blob)
		return sentiment

	def clean_corpus(self, sentences):
		wordlist = []
		for sent in sentences:
			cleaned = Cleaner.clean(sent)
			words = cleaned.split()
			wordlist.extend(words)
		corpus = " ".join(wordlist)
		return corpus

	def generate_ngrams(self, corpus, n):
		corpus = corpus.split(' ')
		output = []
		for i in range(len(corpus)-n+1):
			output.append(" ".join(corpus[i:i+n]))
		return output

	def annotate_language(self, sentence):
		try: 
			lan = detect(sentence)
		except Exception as E:
			lan = "Misc"
		return lan

	def clean_comment(self, comment):
		return Cleaner.clean(comment) # Move this to Self 

	def text_scarsam(self, sentence):
		try:
			sentence = sentence.replace(" ","+")
			url = "http://www.thesarcasmdetector.com/_compute?sentence="+sentence
			resp = requests.get(url)
			return resp.json()['result']
		except:
			return 0

	def get_total_engagements(self, stats):
		return sum(stats.values())

	def parse_fb_time(self,created_time):
		date = created_time.split("T")[0].replace("-","")
		hour = created_time.split("T")[1].split(":")[0]
		day = datetime.datetime(int(date[:4]), int(date[4:6]), int(date[6:])).weekday()
		day = calendar.day_name[day]
		resp = {
			'date' : date,
			'hour' : hour,
			'day' : day
		}			
		return resp

	def parse_cTs(self, cTs):
		created_time = str(time.strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(cTs/1000.)))
		date = created_time.split("T")[0].replace("-","")
		hour = created_time.split("T")[1].split(":")[0]
		
		day = datetime.datetime(int(date[:4]), int(date[4:6]), int(date[6:])).weekday()
		day = calendar.day_name[day]
		resp = {
			'date' : date,
			'hour' : hour,
			'day' : day
		}			
		return resp

	def get_entitiy_used(self, comment_text,entity):
		hashtags = []
		for word in comment_text.split():
			if word.startswith(entity):
				hashtags.append(word)
		return hashtags

	def get_post_sentence_length(self, post):
		blob = TextBlob(post)
		return len(blob.sentences)

class TextGrip:
	def __init__(self):
		self.calais_url = "https://api.thomsonreuters.com/permid/calais"
		self.headers = {'X-AG-Access-Token' : 'Fkzi7dBqweT3T9zogBTmE5U9qwFdx5TY', 'Content-Type' : 'text/raw', 'outputFormat' : 'application/json', 'omitOutputtingOriginalText' : 'true'}


		self.tgu = TextGripUtility()
		self.stopwords = open("Engine/dependencies/stopwords.txt").read().strip().split("\n")
		self.emoticons = open("Engine/dependencies/emoticons.txt").read().strip().split("\n")
		self.meta_data = {
			'power' : open("Engine/dependencies/power.txt").read().strip().split("\n"),
			'emotional' : open("Engine/dependencies/emotional.txt").read().strip().split("\n"),
			'both' : open("Engine/dependencies/both.txt").read().strip().split("\n"),
			'common' : open("Engine/dependencies/common.txt").read().strip().split("\n"),
			'uncommon' : open("Engine/dependencies/uncommon.txt").read().strip().split("\n"),

			'pos_tags' : open("Engine/dependencies/pos_tags.txt").read().strip().split("\n"),
			'positives' : open("Engine/dependencies/positives.txt").read().strip().split("\n"),
			'negatives' : open("Engine/dependencies/negatives.txt").read().strip().split("\n"),
		}

		self.all_tags = ['CommonTag', "Noun", "Verb", "Adverb", "Adjective", "Special"]
		self.pos_dic = {}
		for line in self.meta_data['pos_tags']:
			content = line.split(" ")
			pos = content[0]
			POS = content[1]
			self.pos_dic[pos] = POS

		self.meta_keywords = {}
		for category, data in self.meta_data.iteritems():
			for each in data:
				term = each.split()[0]
				if term not in self.meta_keywords:
					self.meta_keywords[term] = []
				self.meta_keywords[term].append(category)

		for x,y in self.meta_keywords.iteritems():
			if len(y) > 1:
				if y == ['uncommon', 'common']:
					self.meta_keywords[x] = ["uncommon"]
				elif y == ['emotional', 'power', 'both']:
					self.meta_keywords[x] = ["both"]




	def get_pos_tag(self, word):
		blob = TextBlob(word).tags
		return blob

	def get_complete_postags(self,listofsentences):
		complete = {}
		for sent in listofsentences:
			sent_pos = self.get_pos_tag(sent)
			for tags in sent_pos:
				
				word = TextGripUtility().clean_comment(tags[0])
				if not word:
					continue

				if word not in complete:
					complete[word] = []
				if tags[1] not in complete[word]:
					complete[word].append(tags[1])
		return complete

	def get_post_words(self, post_words, key):
		return [x for x in post_words if x in self.meta_data[key]]


	def top_ngrams(self, cleaned_corpus, n = 2):
		ngrams = self.tgu.generate_ngrams(cleaned_corpus, n)
		ngrams = [x for x in ngrams if x.strip() in ngrams]
		ngrams = Counter(ngrams)
		ngrams_dist = sorted(ngrams.items(), key=operator.itemgetter(1), reverse = True)
		return ngrams_dist


	def vocabulary_analysis(self, cleaned_corpus):
		balance = {}
		balance['all'] = cleaned_corpus.split()
		for word in cleaned_corpus.split():
			if word in self.meta_keywords:
				for tag in self.meta_keywords[word]:
					if tag not in balance:
						balance[tag] = []
					balance[tag].append(word)
		for key, values in balance.iteritems():
			vals = Counter(values)
			vals = sorted(vals.items(), key = operator.itemgetter(1), reverse = True)
			balance[key] = vals 
		return balance

	def mentioned_distribution(self, corpus, mentioned):
		mentioned = [x.lower() for x in mentioned]
		dist = {}
		for word in mentioned:
			if word not in dist:
				dist[word] = 0
			dist[word] = corpus.count(word)
		dist = sorted(dist.items(), key = operator.itemgetter(1), reverse = True)
		return dist   

	def optimism_pessimism(self, vocab):
		pos_count = sum([x[1] for x in vocab['positives']])
		neg_count = sum([x[1] for x in vocab['negatives']])
		total = sum([x[1] for x in vocab['all']])

		optimism = round((float(pos_count) / (1+total) * 100),2)
		pessimism = round((float(neg_count) / (1+total) * 100),2)
		return optimism, pessimism

	def emoticon_distribution(self, corpus_raw):
		dist = {}
		for emoline in self.emoticons:
			emo = emoline.split(" :")
			if len(emo) <= 1:
				continue

			emoji = emo[0].strip().strip("'").strip('"')
			sentiment = emo[1].strip().strip("'").strip('"')
			
			cnt = corpus_raw.count(emoji)
			if emoji not in dist:
				dist[emoji] = cnt 
		
		dist = sorted(dist.items(), key = operator.itemgetter(1), reverse = True)
		dist = [x for x in dist if x[1] > 0 and len(x[0]) > 1]

		# Fix Ambiguous Emojis
		ambiguous = ["--"]
		for i, emo in enumerate(dist):
			emoji = emo[0]
			
			temp = dist[:i] + dist[i+1:]
			for tmp_emo in temp:
				temoji = tmp_emo[0]

				if emo[1] == tmp_emo[1]:
					if temoji.lower() in emoji.lower() or emoji.lower() in temoji.lower():
						if len(temoji.lower()) < len(emoji.lower()):
							ambiguous.append(temoji)
						else:
							ambiguous.append(emoji)

		dist = [x for x in dist if x[0] not in ambiguous]
		return dist 


	def pos_tags(self, sentence):
		blob = TextBlob(sentence)
		tags = [self.pos_dic[tag[1]] if tag[1] in self.pos_dic else "CommonTag" for tag in blob.tags]
		
		words_len = len(tags)
		pos_tags_percentage = dict(Counter(tags))
		for tag,freq in pos_tags_percentage.iteritems():
			pos_tags_percentage[tag] = round(float(freq) / words_len,2) * 100
		for y in self.all_tags:
			if y not in pos_tags_percentage:
				pos_tags_percentage[y] = 0
		return pos_tags_percentage

	def phrases(self, proper_corpus):
		blob = TextBlob(proper_corpus)
		noun_phrases = blob.noun_phrases
		nps = Counter(noun_phrases)
		dist = sorted(nps.items(), key = operator.itemgetter(1), reverse = True)
		dist = [x for x in dist if len(x[0].split()) > 1]
		return dist

	def openCalisAPI(self, corpus):
		response = requests.post(self.calais_url, data = corpus, headers = self.headers)
		calis = response.json()
		keys = ['_typeGroup', '_type', 'name', 'score', 'relevance']

		for each in calis:
			for key, value in calis[each].iteritems():
				print key, value

	def top_questions_asked(self, sentences_proper):
		mains = {}
		for sentence in sentences_proper:
			blob = TextBlob(sentence)
			for sent in blob.sentences:
				try:
					words = self.tgu.clean_comment(sent).split()

					# Questions 			
					if len(words) > 3 and sent.endswith("?"):					
						ques = str(sent)
						if ques not in mains:
							mains[ques] = 0
						mains[ques] += 1
	
				except Exception as E:
					continue

				
		dist = sorted(mains.items(), key = operator.itemgetter(1), reverse = True)
		return dist

	def comphrensive_comment(self, sentences_proper):
		mains = {}

		for sentence in sentences_proper:
			blob = TextBlob(sentence)
			slen = len(blob.sentences)

			words = self.tgu.clean_comment(sentence).split()
			wlen = len(words)

			if wlen > 10:
				mains[sentence] = {}
				mains[sentence]["slen"] = slen
				mains[sentence]["wlen"] = wlen

		dicts = [{k: v} for (k,v) in mains.items()]
		dicts.sort(key = lambda d: (d.values()[0]['slen'], d.values()[0]['wlen'],), reverse = True)
		sente = []
		for each in dicts:
			delta = len(str([each.keys()[0]])) - len(each.keys()[0])
			if float(delta) / len(each.keys()[0]) < 1:
				sente.append(each.keys()[0])
		return sente

	def readability_analysis(self, cleaned):
		flesch_reading_ease = textstat.flesch_reading_ease(cleaned)
		smog_index = textstat.smog_index(cleaned)
		flesch_kincaid_grade = textstat.flesch_kincaid_grade(cleaned)	
		coleman_liau_index = textstat.coleman_liau_index(cleaned)
		automated_readability_index = textstat.automated_readability_index(cleaned)
		dale_chall_readability_score = textstat.dale_chall_readability_score(cleaned)
		difficult_words = textstat.difficult_words(cleaned)
		linsear_write_formula = textstat.linsear_write_formula(cleaned)
		gunning_fog = textstat.gunning_fog(cleaned)
		text_standard = textstat.text_standard(cleaned)
		syllable_count = textstat.syllable_count(cleaned)
		avg_syllables_per_word = textstat.avg_syllables_per_word(cleaned)
		avg_letter_per_word = textstat.avg_letter_per_word(cleaned)

		readability = {
			"flesch_reading_ease" : flesch_reading_ease,
			"flesch_kincaid_grade" : flesch_kincaid_grade,
			"text_standard" : text_standard,
			"avg_syllables_per_word" : avg_syllables_per_word,
			"syllable_count" : syllable_count,
			"avg_letter_per_word" : avg_letter_per_word,
			# "smog_index" : smog_index,
			# "coleman_liau_index" : coleman_liau_index,
			# "automated_readability_index" : automated_readability_index,
			# "dale_chall_readability_score" : dale_chall_readability_score,
			# "difficult_words" : difficult_words,
			# "linsear_write_formula" : linsear_write_formula,
			# "gunning_fog" : gunning_fog,
		}
		return readability

tg = TextGrip()
tgu = TextGripUtility()
ge = GenderExtractor()